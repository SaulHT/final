﻿using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using POKER.Controllers;
using POKER.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace POKER.TEST.Controllers
{
    public class PokerControllerTest
    {
        [Test]
        public void mostarJuego()
        {
            var pokers = new PokerController();
            
            var view = pokers.Poker();
            Assert.IsInstanceOf<ViewResult>(view);
        }

        [Test]
        public void ingresarDatosEmpate()
        {
            var dato1 = new Carta[]
            {
                new Carta(){tipoJu=Carta.Tipo.corazones,ValorJu=Carta.Valor.tres},
                new Carta(){ tipoJu=Carta.Tipo.espadas,ValorJu=Carta.Valor.cinco},
                new Carta(){ tipoJu=Carta.Tipo.espadas,ValorJu=Carta.Valor.cinco},
                new Carta(){ tipoJu=Carta.Tipo.diamantes,ValorJu=Carta.Valor.ocho},
                new Carta(){ tipoJu=Carta.Tipo.trebol,ValorJu=Carta.Valor.once}
            };

            var dato2 = new Carta[]
           {
                new Carta(){tipoJu=Carta.Tipo.corazones,ValorJu=Carta.Valor.tres},
                new Carta(){ tipoJu=Carta.Tipo.espadas,ValorJu=Carta.Valor.cinco},
                new Carta(){ tipoJu=Carta.Tipo.espadas,ValorJu=Carta.Valor.cinco},
                new Carta(){ tipoJu=Carta.Tipo.diamantes,ValorJu=Carta.Valor.ocho},
                new Carta(){ tipoJu=Carta.Tipo.trebol,ValorJu=Carta.Valor.once}
           };

        }

        [Test]
        public void ingresarDatosPareja()
        {
            var dato1 = new Carta[]
           {
                new Carta(){tipoJu=Carta.Tipo.corazones,ValorJu=Carta.Valor.tres},
                new Carta(){ tipoJu=Carta.Tipo.espadas,ValorJu=Carta.Valor.cinco},
                new Carta(){ tipoJu=Carta.Tipo.espadas,ValorJu=Carta.Valor.cinco},
                new Carta(){ tipoJu=Carta.Tipo.diamantes,ValorJu=Carta.Valor.ocho},
                new Carta(){ tipoJu=Carta.Tipo.trebol,ValorJu=Carta.Valor.once}
           };

            var dato2 = new Carta[]
           {
                new Carta(){tipoJu=Carta.Tipo.corazones,ValorJu=Carta.Valor.tres},
                new Carta(){ tipoJu=Carta.Tipo.espadas,ValorJu=Carta.Valor.cinco},
                new Carta(){ tipoJu=Carta.Tipo.espadas,ValorJu=Carta.Valor.cinco},
                new Carta(){ tipoJu=Carta.Tipo.diamantes,ValorJu=Carta.Valor.ocho},
                new Carta(){ tipoJu=Carta.Tipo.trebol,ValorJu=Carta.Valor.once}
           };


        }
        [Test]
        public void ingresarDatosescaleraColor()
        {
            var dato1 = new Carta[]
           {
                new Carta(){tipoJu=Carta.Tipo.corazones,ValorJu=Carta.Valor.tres},
                new Carta(){ tipoJu=Carta.Tipo.corazones,ValorJu=Carta.Valor.cuatro},
                new Carta(){ tipoJu=Carta.Tipo.corazones,ValorJu=Carta.Valor.cinco},
                new Carta(){ tipoJu=Carta.Tipo.corazones,ValorJu=Carta.Valor.seis},
                new Carta(){ tipoJu=Carta.Tipo.corazones,ValorJu=Carta.Valor.siete}
           };

            var dato2 = new Carta[]
           {
                new Carta(){tipoJu=Carta.Tipo.corazones,ValorJu=Carta.Valor.tres},
                new Carta(){ tipoJu=Carta.Tipo.espadas,ValorJu=Carta.Valor.cinco},
                new Carta(){ tipoJu=Carta.Tipo.espadas,ValorJu=Carta.Valor.cinco},
                new Carta(){ tipoJu=Carta.Tipo.diamantes,ValorJu=Carta.Valor.ocho},
                new Carta(){ tipoJu=Carta.Tipo.trebol,ValorJu=Carta.Valor.once}
           };
        }
    }
}
