﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POKER.Models
{
    public enum mano
    {
        Nada,
        Carta_alta,
        Un_par,
        Dos_pares,
        ThreeKind,
        Derecho,
        Flush,
        Casa_llena,
        FourKind,
        Escalera
    }

    public struct valorMano
    {
        public int total { get; set; }
        public int cartaAlta { get; set; }
    }
    public class EvaluarCarta:Carta
    {

            private int sumCorazones;
            private int sumDiamat;
            private int sumTrebol;
            private int sumEspada;
            private Carta[] cartas;
            public valorMano valorMano;
           
           
        public EvaluarCarta(Carta[] ordenado)
        {
            sumCorazones = 0;
            sumDiamat = 0;
            sumEspada = 0;
           sumTrebol = 0;
            cartas = new Carta[5];
            cartas = ordenado;
            valorMano = new valorMano();

        }

        // propiedad por valor manual
        public valorMano valorManos
        {
            get { return valorMano; }
            set { valorMano = value; }
        }

        //property for cards array
        public Carta[] Cartas
        {
            get { return cartas; }
            set
            {
                cartas[0] = value[0];
                cartas[1] = value[1];
                cartas[2] = value[2];
                cartas[3] = value[3];
                cartas[4] = value[4];
            }
        }

        //
        public mano Evaluar_mano()
        {
            //get the number of each suit on hand (for checking flush/5 cards of same suit)
            getNumeroTraje();
            //Iterate checks through each possible hand from strongest to weakest to avoid false positive
           // if (Escalera())
           //     return mano.Escalera;
            if (FourKind())
                return mano.FourKind;
            else if (Casa_llena())
                return mano.Casa_llena;
            else if (Flush())
                return mano.Flush;
            else if (Derecho())
                return mano.Derecho;
            else if (tresCartas())
                return mano.ThreeKind;
            else if (Dos_pares())
                return mano.Dos_pares;
            else if (Un_par())
                return mano.Un_par;

            //if the hand is nothing, return HighCard as default
            valorMano.cartaAlta = (int)cartas[4].ValorJu;

            return mano.Nada;

        }

        // Get total number of each suit in hand for flush check
        private void getNumeroTraje()
        {
            foreach (var element in Cartas)
            {
                if (element.tipoJu == Carta.Tipo.corazones)
                    sumCorazones++;
                else if (element.tipoJu == Carta.Tipo.diamantes)
                    sumDiamat++;
                else if (element.tipoJu == Carta.Tipo.trebol)
                    sumTrebol++;
                else if (element.tipoJu == Carta.Tipo.espadas)
                    sumEspada++;
            }
        }


        private bool FourKind()
        {
            //if the first 4 cards, add values of the four cards and last card is the highest
            if (cartas[0].ValorJu == cartas[1].ValorJu && cartas[0].ValorJu == cartas[2].ValorJu && cartas[0].ValorJu == cartas[3].ValorJu)
            {
                valorMano.total = (int)cartas[1].ValorJu * 4;
                valorMano.cartaAlta = (int)cartas[4].ValorJu;
                return true;
            }
            else if (cartas[1].ValorJu == cartas[2].ValorJu && cartas[1].ValorJu == cartas[3].ValorJu && cartas[1].ValorJu == cartas[4].ValorJu)
            {
                valorMano.total = (int)cartas[1].ValorJu * 4;
                valorMano.cartaAlta = (int)cartas[0].ValorJu;
                return true;
            }

            return false;
        }

        private bool Casa_llena()
        {
            if ((cartas[0].ValorJu == cartas[1].ValorJu && cartas[0].ValorJu == cartas[2].ValorJu && cartas[3].ValorJu == cartas[4].ValorJu) ||
               (cartas[0].ValorJu == cartas[1].ValorJu && cartas[2].ValorJu== cartas[3].ValorJu && cartas[2].ValorJu == cartas[4].ValorJu))
            {
                valorMano.total = (int)(cartas[0].ValorJu) + (int)(cartas[1].ValorJu) + (int)(cartas[2].ValorJu) +
                    (int)(cartas[3].ValorJu) + (int)(cartas[4].ValorJu);
                return true;
            }

            return false;
        }

        private bool Flush()
        {
            //if all suits are the same
            if (sumCorazones == 5 || sumDiamat == 5 || sumTrebol == 5 || sumEspada == 5)
            {
                //if flush, the player with higher cards win
                //whoever has the last card the highest, has automatically all the cards total higher
                valorMano.total = (int)cartas[4].ValorJu;
                return true;
            }

            return false;
        }

        private bool Derecho()
        {
            //if 5 consecutive values
            if (cartas[0].ValorJu + 1 == cartas[1].ValorJu &&
                cartas[1].ValorJu + 1 == cartas[2].ValorJu &&
                cartas[2].ValorJu + 1 == cartas[3].ValorJu &&
                cartas[3].ValorJu + 1 == cartas[4].ValorJu)
            {
                //player with the highest value of the last card wins
                valorMano.total = (int)cartas[4].ValorJu;
                return true;
            }

            return false;

        }

        private bool tresCartas()
        {
            //if 3 cards are the same
            if ((cartas[0].ValorJu == cartas[1].ValorJu && cartas[0].ValorJu == cartas[2].ValorJu) ||
             (cartas[1].ValorJu == cartas[2].ValorJu && cartas[1].ValorJu == cartas[3].ValorJu))
            {
                valorMano.total = (int)cartas[2].ValorJu * 3;
                valorMano.cartaAlta = (int)cartas[4].ValorJu;
                return true;
            }
            else if (cartas[2].ValorJu == cartas[3].ValorJu && cartas[2].ValorJu == cartas[4].ValorJu)
            {
                valorMano.total = (int)cartas[2].ValorJu * 3;
                valorMano.cartaAlta = (int)cartas[1].ValorJu;
                return true;
            }
            return false;
        }

        private bool Dos_pares()
        {
            
            if (cartas[0].ValorJu == cartas[1].ValorJu && cartas[2].ValorJu == cartas[3].ValorJu)
            {
                valorMano.total = ((int)cartas[1].ValorJu * 2) + ((int)cartas[3].ValorJu * 2);
                valorMano.cartaAlta = (int)cartas[4].ValorJu;
                return true;
            }
            else if (cartas[0].ValorJu == cartas[1].ValorJu && cartas[3].ValorJu == cartas[4].ValorJu)
            {
                valorMano.total = ((int)cartas[1].ValorJu * 2) + ((int)cartas[3].ValorJu * 2);
                valorMano.cartaAlta = (int)cartas[2].ValorJu;
                return true;
            }
            else if (cartas[1].ValorJu == cartas[2].ValorJu && cartas[3].ValorJu == cartas[4].ValorJu)
            {
                valorMano.total = ((int)cartas[1].ValorJu * 2) + ((int)cartas[3].ValorJu * 2);
                valorMano.cartaAlta = (int)cartas[0].ValorJu;
                return true;
            }
            return false;
        }

        private bool Un_par()
        {
            //if pair of cards return single value, easiest to program!
            if (cartas[0].ValorJu == cartas[1].ValorJu)
            {
                valorMano.total = (int)cartas[0].ValorJu * 2;
                valorMano.cartaAlta = (int)cartas[4].ValorJu;
                return true;
            }
            else if (cartas[1].ValorJu == cartas[2].ValorJu)
            {
                valorMano.total = (int)cartas[1].ValorJu * 2;
                valorMano.cartaAlta = (int)cartas[4].ValorJu;
                return true;
            }
            else if (cartas[2].ValorJu == cartas[3].ValorJu)
            {
                valorMano.total = (int)cartas[2].ValorJu * 2;
                valorMano.cartaAlta = (int)cartas[4].ValorJu;
                return true;
            }
            else if (cartas[3].ValorJu == cartas[4].ValorJu)
            {
                valorMano.total = (int)cartas[3].ValorJu * 2;
                valorMano.cartaAlta = (int)cartas[2].ValorJu;
                return true;
            }

            return false;

        }
    }
}
