﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POKER.Models
{
    public class Carta
    {
       public enum Valor
        {
            dos=2,tres,cuatro,cinco,seis,siete,ocho,
            nueve,diez,once,doce,trece, Ace
        }

        public enum Tipo
        {
            corazones,
            espadas,
            diamantes,
            trebol
        }

        public Valor ValorJu { get; set; }
        public Tipo tipoJu { get; set; }
    }
}
