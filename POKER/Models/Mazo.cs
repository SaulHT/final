﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POKER.Models
{
    public class Mazo:Carta
    {
        const int numCarta = 52;
        private Carta[] cubierto;

        public Mazo()
        {
            cubierto = new Carta[numCarta];
        }

        public Carta[] getCubierto { get { return cubierto; } }//obtener mazo actual

        public void crearMazo_Barajarlo()
        {
            int i = 0;
            foreach (Tipo s in Enum.GetValues(typeof(Tipo)))
            {
                foreach (Valor v in Enum.GetValues(typeof(Valor)))
                {
                    cubierto[i] = new Carta{ tipoJu = s, ValorJu = v };
                    i++;
                }
            }
            barajarCartas();
        }

        public void barajarCartas()
        {
            Random rand = new Random();
            Carta temp;

            for (int  baraja= 0;  baraja< 100; baraja++)
            {
                for (int i = 0; i <numCarta ; i++)
                {
                    int segCarta = rand.Next(13);
                    temp = cubierto[i];
                    cubierto[i] = cubierto[segCarta];
                    cubierto[segCarta] = temp;
                }
            }
        }
    }
}
