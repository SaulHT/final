﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POKER.Models
{
    public class Juego
    {
        public string[] playermano { get; set; }
        public string[] cpumano { get; set; }
        public int resultado { get; set; }
        public string playerResult { get; set; }
        public string cpuResultado { get; set; }
    }
}
