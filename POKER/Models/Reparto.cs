﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static POKER.Models.EvaluarCarta;

namespace POKER.Models
{
    public class Reparto : Mazo
    {
        public Carta[] ManoJueg;
        public Carta[] ManoComputer;
        public Carta[] ManojugadorOrdena;
        public Carta[] ManoOrdenaComput;
        public mano jugadorGana;
        public mano CpuGanado;
        public int resultado;

        public Reparto()
        {
            ManoJueg = new Carta[5];
            ManojugadorOrdena = new Carta[5];
            ManoComputer = new Carta[5];
            ManoOrdenaComput = new Carta[5];
           
        }
        public void Acuerdo()
        {
            crearMazo_Barajarlo(); 
            manoReparto(); 
            clasificarCarta(); 
            EvaluarMano(); 
        }
        public void manoReparto()
        {
            // Repartir 2 cartas para el jugador
            // 5 cartas para el jugador
            for (int i = 0; i < 5; i++)
                ManoJueg[i] = getCubierto[i];

            //5 cartas para jugador2
            for (int i = 5; i < 10; i++)
                ManoComputer[i - 5] = getCubierto[i];
        }

        
        public void clasificarCarta()
        {
            var QueryPlayer = from mano in ManoJueg
                              orderby mano.ValorJu
                              select mano;
            var QueryComputer = from mano in ManoComputer
                                orderby mano.ValorJu
                                select mano;
            var index = 0;

            foreach (var element in QueryPlayer.ToList())
            {
                ManojugadorOrdena[index] = element;
                index++;
            }
            index = 0;
            foreach (var element in QueryComputer.ToList())
            {
                ManoOrdenaComput[index] = element;
                index++;
            }
        }

        public string[] cambiarNombreMano(Carta[] mano)
        {
            string[] newMano = new string[mano.Length];
            int i = 0;
            foreach (var card in mano)
            {
                newMano[i] = (Convert.ToString(card.ValorJu)) + " de " + (Convert.ToString(card.tipoJu));

                i++;
            }
            return newMano;
        }

        public void EvaluarMano()
        {
            
            EvaluarCarta playerHandEvaluator = new EvaluarCarta(ManojugadorOrdena);
            EvaluarCarta computerHandEvaluator = new EvaluarCarta(ManoOrdenaComput);
            

            //get the player;s and computer's hand
            mano playerHand = playerHandEvaluator.Evaluar_mano();
            mano computerHand = computerHandEvaluator.Evaluar_mano();

            

            jugadorGana = playerHand;
            CpuGanado = computerHand;

          
            if (playerHand > computerHand)
            {
                resultado = 1;
            }
            else if (playerHand < computerHand)
            {
                resultado = 0;
            }
            else


            //if the hands are the same, evaluate the values
            {
                //first evaluate who has higher value of poker hand
                if (playerHandEvaluator.valorMano.total > computerHandEvaluator.valorMano.total)
                {
                    resultado = 1;
                }

                else if (playerHandEvaluator.valorMano.total < computerHandEvaluator.valorMano.total)
                {
                    resultado = 0;
                }
                //if both have the same poker hand (for example, both have a pair of queens), 
                //then the player with the next higher card wins
                else if (playerHandEvaluator.valorMano.total > computerHandEvaluator.valorMano.total)
                    resultado = 1;
                else if (playerHandEvaluator.valorMano.total < computerHandEvaluator.valorMano.total)
                    resultado = 0;                //if high card is of same value check second card in players hand
                else if (playerHandEvaluator.valorMano.cartaAlta > computerHandEvaluator.valorMano.cartaAlta)
                    resultado = 1;
                else if (playerHandEvaluator.valorMano.cartaAlta < computerHandEvaluator.valorMano.cartaAlta)
                    resultado = 0;
                else
                {
                    resultado = 2;
                }
            }
        }
    }
}
