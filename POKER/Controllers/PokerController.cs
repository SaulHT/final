﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using POKER.Models;

namespace POKER.Controllers
{
    public class PokerController : Controller
    {
        
        public IActionResult Index()
        {
           
            return View();
        }
        public IActionResult Poker()
        {
            Reparto repart = new Reparto();
            repart.Acuerdo();

            Juego dato = new Juego();
 
            dato.playermano = repart.cambiarNombreMano(repart.ManojugadorOrdena);
            dato.cpumano = repart.cambiarNombreMano(repart.ManoOrdenaComput);
            dato.resultado = repart.resultado;


            return View(dato);
        }

    }
}
